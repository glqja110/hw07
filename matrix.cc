#include "matrix.h"

using namespace std;

vector<int> valuess;

Matrix::Matrix() {
	rows_=0;
	cols_=0;
}

Matrix::Matrix(int rows, int cols) {
	rows_=rows;
	cols_=cols;
	Resize(rows,cols);    
}

void Matrix::putNum() {
	for(int i=0;i<cols_;i++) {
		for(int j=0;j<rows_;j++) {
			values_[rows_*i+j]=valuess[i+cols_*j];
		}
	}
	valuess.clear();
}


int& Matrix::operator()(int r, int c) {
	return values_[Sub2Ind(r, c)];               //r행 c열에 있는 원소 반환
}

const int& Matrix::operator()(int r, int c) const {
	return values_[Sub2Ind(r, c)]; 
}


Matrix Matrix::operator+() const {  // 단항연산자 (Unary operators)
	Matrix pos(rows_,cols_);
	valuess.resize(rows_*cols_);
	for(int i=0;i<rows_*cols_;i++) {
			valuess[i]=values_[i];
	}
	pos.putNum();
	return pos;
}

Matrix Matrix::operator-() const { // ex) Matrix m(2,3); cout << -m;
	Matrix pos(rows_,cols_);
	valuess.resize(rows_*cols_);
	int k=0;
	for(int i=0;i<rows_;i++) {
		for(int j=0;j<cols_;j++) {
			valuess[k]=(values_[rows_*j+i])*(-1);
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix Matrix::Transpose() const {  // 아래 설명 참조.
	Matrix pos(cols_,rows_);
	valuess.resize(rows_ * cols_);
	for(int i=0;i<rows_*cols_;i++)
		valuess[i]=values_[i];
	pos.putNum();
	valuess.clear();
	return pos;
}

//밑으로 class 바깥에 있는 애들

istream& operator>>(istream& is, Matrix& m) {
	int row, col;
	is>>row>>col;        //행 열을 먼저 입력 받음
	valuess.resize((row) * (col));
	m.Resize(row,col);
	for(int i=0;i<row*col;i++)
		is>>valuess[i];	
	m.putNum();
	valuess.clear();
	return is;
}

ostream& operator<<(ostream& os, const Matrix& m) {
	for(int i=0;i<m.rows();i++) {
		for(int j=0;j<m.cols();j++) {
			os<<m(i,j)<<' ';
		}
		os<<endl;
	}
	return os;
}

Matrix operator+(const Matrix& lm, const Matrix& rm) {
	if(lm.rows()!=rm.rows()||lm.cols()!=rm.cols()) {
		cout << "Invalid operation" << endl;
		exit(0);
	}
	Matrix pos(lm.rows(),lm.cols());
	valuess.resize(lm.rows()*lm.cols());
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<lm.cols();j++) {
			valuess[lm.cols()*i+j]=lm(i,j)+rm(i,j);
		}
	}
	
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator-(const Matrix& lm, const Matrix& rm) {
	if(lm.rows()!=rm.rows()||lm.cols()!=rm.cols()) {
		cout << "Invalid operation" << endl;		
		exit(0);
	}
	Matrix pos(lm.rows(),lm.cols());
	valuess.resize(lm.rows()*lm.cols());
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<lm.cols();j++) {
			valuess[lm.cols()*i+j]=lm(i,j)-rm(i,j);
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator*(const Matrix& lm, const Matrix& rm) {
	Matrix pos(lm.rows(),rm.cols());
	valuess.resize(lm.rows()*rm.cols());
	int a=0;
	int tmp=0;
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<rm.cols();j++) {
			for(int k=0; k<rm.rows();k++) {
				tmp+=lm(i,k)*rm(k,j);
			}
			valuess[a]=tmp;
			a++;
			tmp=0;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator+(const int& a, const Matrix& rm) {
	Matrix pos(rm.rows(),rm.cols());
	valuess.resize(rm.rows()*rm.cols());
	int k=0;
	for(int i=0;i<rm.rows();i++) {
		for(int j=0;j<rm.cols();j++) {
			valuess[k]=a+rm(i,j);
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator-(const int& a, const Matrix& rm) {
	Matrix pos(rm.rows(),rm.cols());
	valuess.resize(rm.rows()*rm.cols());
	int k=0;
	for(int i=0;i<rm.rows();i++) {
		for(int j=0;j<rm.cols();j++) {
			valuess[k]=a-rm(i,j);
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator*(const int& a, const Matrix& rm) {
	Matrix pos(rm.rows(),rm.cols());
	valuess.resize(rm.rows()*rm.cols());
	int k=0;
	for(int i=0;i<rm.rows();i++) {
		for(int j=0;j<rm.cols();j++) {
			valuess[k]=a*rm(i,j);
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator+(const Matrix& lm, const int& a) {
	Matrix pos(lm.rows(),lm.cols());
	valuess.resize(lm.rows()*lm.cols());
	int k=0;
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<lm.cols();j++) {
			valuess[k]=lm(i,j)+a;
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator-(const Matrix& lm, const int& a) {
	Matrix pos(lm.rows(),lm.cols());
	valuess.resize(lm.rows()*lm.cols());
	int k=0;
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<lm.cols();j++) {
			valuess[k]=lm(i,j)-a;
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}

Matrix operator*(const Matrix& lm, const int& a) {
	Matrix pos(lm.rows(),lm.cols());
	valuess.resize(lm.rows()*lm.cols());
	int k=0;
	for(int i=0;i<lm.rows();i++) {
		for(int j=0;j<lm.cols();j++) {
			valuess[k]=a*lm(i,j);
			k++;
		}
	}
	pos.putNum();
	valuess.clear();
	return pos;
}
