#include "draw_shape.h" 


//OK
Canvas::Canvas(size_t row, size_t col) {                            
	row_=row;
	col_=col;
	canvas_.reserve(col_);
	for(int i=0; i<col_;i++)                //canvas를 입력한 크기만큼 정해줌
		canvas_[i].resize(row_,"."); 
}
Canvas::~Canvas() {} 

  // Canvas  크기를 w x h  로  변경한다.  그려진  내용은  보존한다. 
void Canvas::Resize(size_t w, size_t h) {
	row_=w;
	col_=h;
	canvas_.reserve(col_);
	for(int i=0; i<col_;i++)                //canvas를 입력한 크기만큼 정해줌
		canvas_[i].resize(row_,"."); 
}
 
  // (x,y)  위치에 ch  문자를  그린다.  범위  밖의 x,y  는  무시한다. 
bool Canvas::Draw(int x, int y, char brush)  {
	if(x<0||y<0||x>=row_||y>=col_) return false;
	else {
		canvas_[y][x]=brush;
		return true;
	}
}

 
  //  그려진  내용을  모두  지운다 ('.' 으로  초기화) 
//OK
void Canvas::Clear() {                                                 
	for(int i=0;i<col_;i++) {
		for(int j=0;j<row_;j++) {
			canvas_[i][j]=".";
		}
	}
}

string Canvas::point(int x,int y) {
	return canvas_[y][x];
}

ostream& operator<<(ostream& os, Canvas& c) {
	os<<" ";
	for(int i=0;i<c.row();i++) {
		if(c.row()<10)
		os<<i;
		else
			os<<(i%10);
	}
	os<<endl;
	for(int i=0;i<c.col();i++) {
		if(c.col()<10)
		os<<i;
		else
			os<<(i%10);
		for(int j=0;j<c.row();j++)
			os<<c.point(j,i);
		os<<endl;
	}	
	return os;
}


istream& operator>>(istream& is, Rectangle& r) {        //rect x y width height brush
	int x,y,width,height;
	char brush;
	is>>x>>y>>width>>height>>brush;
	r.setR(x,y,width,height,brush);
	return is;
}

istream& operator>>(istream& is, UpTriangle& t) {         //tri_up x y height brush
	int x,y,height;
	char brush;
	is>>x>>y>>height>>brush;
	t.setU(x,y,height,brush);
	return is;
}

istream& operator>>(istream& is, DownTriangle& d) {         //tri_down x y height brush
	int x,y,height;
	char brush;
	is>>x>>y>>height>>brush;
	d.setD(x,y,height,brush);
	return is;
}

istream& operator>>(istream& is, Diamond& dm) {         //rect x y width height brush
	int x,y,height;
	char brush;
	is>>x>>y>>height>>brush;
	dm.setDm(x,y,height,brush);
	return is;
}
