#ifndef _DRAW_SHAPE_H_
#define _DRAW_SHAPE_H_

#include <iostream> 
#include <string> 
#include <vector> 


using namespace std;


class Canvas { 
 public: 
  Canvas(size_t row, size_t col);  //처음 캔버스 크기를 정함
  ~Canvas(); 
 
  // Canvas  크기를 w x h  로  변경한다.  그려진  내용은  보존한다. 
  void Resize(size_t w, size_t h); 
 
  // (x,y)  위치에 ch  문자를  그린다.  범위  밖의 x,y  는  무시한다. 
  bool Draw(int x, int y, char brush); 
 
  //  그려진  내용을  모두  지운다 ('.' 으로  초기화) 
  void Clear(); 
  int row() {return row_;}
  int col() {return col_;}
  string point(int x,int y);
  private: 
 	int row_;
	int col_;
	vector<vector<string> > canvas_;
  //  그려진  모양을  저장할  수  있도록  데이터멤버를  정의 (resize  가능에  주의) 
  friend ostream& operator<<(ostream& os, Canvas& c); 
}; 

class Shape { 
 public: 
  virtual ~Shape() {}; 
  virtual void Draw(Canvas* canvas) const = 0; 
  virtual string type() {return type_;}
  virtual int x() {return x_;}
  virtual int y() {return y_;}
  virtual int w() {return width_;}
  virtual int h() {return height_;}
  virtual char brush() {return brush_;}
 protected: 
  //  도형의  공통  속성을  정의. 
	int x_;
	int y_;
	int width_;
	int height_;
	char brush_;
	string type_;
}; 

class Rectangle : public Shape {
  public:
	Rectangle() {
		x_=0;
		y_=0;
		width_=0;
		height_=0;
		brush_=' ';
		type_="rect";
	}
	void setR(int x,int y,int width,int height,char brush) {
		x_=x;
		y_=y;
		width_=width;
		height_=height;
		brush_=brush;
	}
	~Rectangle() { }
	virtual void Draw(Canvas* canvas) const{
		for(int i=y_;i<y_+height_;i++) {
			for(int j=x_;j<x_+width_;j++) 
				if(canvas->Draw(j,i,brush_)) {};
		}
	}

};
 
class UpTriangle : public Shape {
  public:
	UpTriangle() {
		x_=0;
		y_=0;
		height_=0;
		brush_=' ';
		type_="tri_up";
		
	}
	void setU(int x,int y,int H,char brush) {
		x_=x;
		y_=y;
		height_=H;
		brush_=brush;
	}
	~UpTriangle() { }
	virtual void Draw(Canvas* canvas) const{
		int side=0;
		for(int i=y_;i<(y_+height_);i++) {
			for(int j=(x_-side);j<=(x_+side);j++) {
				if(canvas->Draw(j,i,brush_)) {};
			}
			side++;
		}
	}

};
 
class DownTriangle : public Shape {
  public:
	DownTriangle() {
		x_=0;
		y_=0;
		height_=0;
		brush_=' ';
		type_="tri_down";
	}
	void setD(int x,int y,int H,char brush) {
		x_=x;
		y_=y;
		height_=H;
		brush_=brush;
	}	
	~DownTriangle() { }
	virtual void Draw(Canvas* canvas) const{
		int side=0;
		for(int i=y_;i>y_-height_;i--) {
			for(int j=x_-side;j<=x_+side;j++) {
				if(canvas->Draw(j,i,brush_)) {};
				
			}
			side++;
		}
	}

 		
};
	 
class Diamond : public Shape {
  public:
	Diamond() {
		x_=0;
		y_=0;
		height_=0;
		brush_=' ';
		type_="diamond";
	}
	void setDm(int x,int y,int H,char brush) {
		x_=x;
		y_=y;
		height_=H;
		brush_=brush;
	}	
	~Diamond() { }
	virtual void Draw(Canvas* canvas) const{
		int side=0;
		for(int i=y_;i<=(y_+height_);i++) {
			for(int j=(x_-side);j<=(x_+side);j++) {
				if(canvas->Draw(j,i,brush_)) {};
			}
			side++;
		}
		side-=2;
		for(int i=y_+height_+1;i<=y_+2*height_;i++) {
			for(int j=(x_-side);j<=(x_+side);j++) {
				if(canvas->Draw(j,i,brush_)) {};
			}
			side--;
		}
	}

 
};
 
istream& operator>>(istream& is, Rectangle& r); 
istream& operator>>(istream& is, UpTriangle& t); 
istream& operator>>(istream& is, DownTriangle& d); 
istream& operator>>(istream& is, Diamond& dm);

#endif /* matrix.h */
