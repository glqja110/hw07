#include "unistd.h" 
#include <algorithm> 
#include <iostream> 
#include <vector> 

using namespace std; 

const int kNumCards = 100; 
const int kNumSets = 10; 
const int kFirstCard = 5; 


int main() {
	vector<int> CardList(kNumSets);               //10개짜리 벡터를 준비한다.
	for(int i=0;i<10;i++)          //순서대로 집어넣는다
		CardList[i]=i;
	int opponent_card, my_score, opponent_score; 
	for(int i=0;i<kNumSets;i++) {
		int first=10*CardList[i]+5;         // 5 15 25 35 45 55 65 75 85 95    (0~4와 96~100은 후의 최대한 이득을 위해 상대가 최고로 큰 숫자를 꺼내기 전까지는 사용 안함)
		cout<<first<<endl;
		cin >> opponent_card >> my_score >> opponent_score; 	
		int second;
		//50 이런식으로 나머지 0일때
		if(opponent_card/10==10&&opponent_card%10==0) {    //상대 카드가 91~100 짜리면
			for(int j=1;j<5;j++) {                    //1~4로 피해 최소화 하고
				cout<<j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
			for(int j=100;j>95;j--) {    //100 99 98 97 96 으로 공격을 해서 최대한의 이득을 본다
				cout<<j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==9&&opponent_card%10==0) {    //상대 카드가 81~90 짜리면
			second=10*(opponent_card/10-1)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==8&&opponent_card%10==0) {  //상대 카드가 71~80 짜리면
			second=10*(opponent_card/10-1)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==7&&opponent_card%10==0) {  //상대 카드가 61~70 짜리면
			second=10*(opponent_card/10-1)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==6&&opponent_card%10==0) {  //상대 카드가 51~60 짜리면
			second=10*(opponent_card/10-1)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}

		else if(opponent_card/10==5&&opponent_card%10==0) {  //상대 카드가 41~50 짜리면
			second=10*(opponent_card/10-1)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==4&&opponent_card%10==0) {     //상대 카드가 31~40 짜리면
			second=10*(opponent_card/10-1)+6;    //46 ~ 54까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==3&&opponent_card%10==0) {  //상대 카드가 21~30 짜리면
			second=10*(opponent_card/10-1)+6;    //36 ~ 44까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==2&&opponent_card%10==0) {     //상대 카드가 11~20 짜리면
			second=10*(opponent_card/10-1)+6;    //26 ~ 34까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==1&&opponent_card%10==0) {   //상대 카드가 1~10 짜리면
			second=10*(opponent_card/10-1)+6;    //16 ~ 24까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		//55 이런식으로 나머지 5일때 
		else if(opponent_card/10==9&&opponent_card%10==5) {    //상대 카드가 91~100 짜리면
			for(int j=1;j<5;j++) {                    //1~4로 피해 최소화 하고
				cout<<j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
			for(int j=100;j>95;j--) {    //100 99 98 97 96 으로 공격을 해서 최대한의 이득을 본다
				cout<<j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==8&&opponent_card%10==5) {  //상대 카드가 81~90 짜리면
			second=10*(opponent_card/10)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==7&&opponent_card%10==5) {  //상대 카드가 71~80 짜리면
			second=10*(opponent_card/10)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==6&&opponent_card%10==5) {  //상대 카드가 61~70 짜리면
			second=10*(opponent_card/10)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}

		else if(opponent_card/10==5&&opponent_card%10==5) {  //상대 카드가 51~60 짜리면
			second=10*(opponent_card/10)+6;       //86~94까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==4&&opponent_card%10==5) {     //상대 카드가 41~50 짜리면
			second=10*(opponent_card/10)+6;    //46 ~ 54까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==3&&opponent_card%10==5) {  //상대 카드가 31~40 짜리면
			second=10*(opponent_card/10)+6;    //36 ~ 44까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==2&&opponent_card%10==5) {     //상대 카드가 21~30 짜리면
		second=10*(opponent_card/10)+6;    //26 ~ 34까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==1&&opponent_card%10==5) {   //상대 카드가 11~20 짜리면
		second=10*(opponent_card/10)+6;    //16 ~ 24까지로 최대한 이득 볼 것임
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
		else if(opponent_card/10==0&&opponent_card%10==5) {   //상대 카드가 1~10 짜리면
		second=10*(opponent_card/10)+6;    //6 ~ 14까지로 최대한 이득 볼 것임 
			for(int j=0;j<9;j++) {
				cout<<second+j<<endl;
				cin >> opponent_card >> my_score >> opponent_score;
			}
		}
	}

}
	


